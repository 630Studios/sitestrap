<?php
/**
 * Created by PhpStorm.
 * User: Jason
 * Date: 3/31/2018
 * Time: 12:37 AM
 */
require_once "ColorData.php";
require_once "data.php";

require_once "templates/alerts.php";

class CssGenerator
{


    static function Generate()
    {
        $SiteColors = $GLOBALS['SiteColors'];
        $output = '';

        foreach ($SiteColors as $color)
        {
            $output = $output . '--sitestrap-' . $color->name . ':' . $color->color . ";\r\n";
            for ($i = 1; $i < 5; $i++) {
                $l1 = self::adjustBrightness($color->color, -15 * $i);
                $l2 = self::adjustBrightness($color->color, 15 * $i);
                $output = $output . '--sitestrap-' . $color->name . "-$i:" . $l1 . ";\r\n";
                $output = $output . '--sitestrap-' . $color->name . "+$i:" . $l2 . ";\r\n";
            }
        }

        $ColorThemes = $GLOBALS['ColorThemes'];

        foreach ($ColorThemes as $colorTheme)
        {
            echo GenerateAlert($colorTheme) . "\r\n";
        }
        echo $output;
    }


    static function adjustBrightness($hex, $steps) {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Split into three parts: R, G and B
        $color_parts = str_split($hex, 2);
        $return = '#';

        foreach ($color_parts as $color) {
            $color   = hexdec($color); // Convert to decimal
            $color   = max(0,min(255,$color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }

}

CssGenerator::Generate();