<?php
/**
 * Created by PhpStorm.
 * User: Jason
 * Date: 3/31/2018
 * Time: 12:39 AM
 */

class ColorThemeData
{
    public $name;
    public $backgroundColor;
    public $borderColor;
    public $textColor;
    public $color_4;

    public function __construct($mName, $mBgCol, $mBorderCol, $mTextCol, $mColor4)
    {
        $this->name = $mName;
        $this->backgroundColor = $mBgCol;
        $this->borderColor = $mBorderCol;
        $this->textColor = $mTextCol;
        $this->color_4 = $mColor4;
    }


    function backgroundVariable()
    {
        return "sitestrap--" . $this->backgroundColor;
    }

    function borderVariable()
    {
        return "sitestrap--" . $this->borderColor;
    }

    function textVariable()
    {
        return "sitestrap--" . $this->textColor;
    }
}