<?php
require_once 'ColorData.php';

function GenerateAlert($colorTheme)
{
    $AlertTemplate = "
.alert-$colorTheme->name{

    background-color: var(" . $colorTheme->backgroundVariable() . ");
    border-color: var(" . $colorTheme->borderVariable() . ");
    color:var(" . $colorTheme->textVariable() .");
}

.alert-serious > .icon{
    color: var(" . $colorTheme->backgroundVariable() . ");
    background-color:var(" . $colorTheme->textVariable() . ");
}
";

    return $AlertTemplate;
}








