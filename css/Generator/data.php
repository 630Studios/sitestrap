<?php
/**
 * Created by PhpStorm.
 * User: Jason
 * Date: 3/31/2018
 * Time: 12:36 AM
 */
require_once "ColorData.php";
require_once "ColorThemeData.php";




$GLOBALS['SiteColors'] = array(
    new ColorData("white", "#FFFFFF"),
    new ColorData("black", "#000000"),
    new ColorData("light-grey", "#FFFFFF"),
    new ColorData("light-grey", "#FFFFFF"),
);

$GLOBALS['ColorThemes'] = array(
    new ColorThemeData("serious", "red", "light-red", "dark-red", "black")
);



function GetColorByName($name)
{
    $SiteColors = $GLOBALS['SiteColors'];

    foreach($SiteColors as $color)
    {
        if ($color->name == $name)
            return $color;
    }
}
